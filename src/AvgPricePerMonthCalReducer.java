
/**
 * @author ajay subramanya & smitha bangalore naresh
 * @date 01/29/2016
 * @info Assignment 3
 * Reducer class -
 * Gets called for each Airline and data is arranged by Month using partitioner and
 * group sorting. Values are checked if flight is active for 2015 and average for
 * each month is calculated and flight frequency is calculated.
 * Output : AirlineName, (month, avg price)..., frequency
 * */
import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class AvgPricePerMonthCalReducer extends Reducer<AirlineMonthCompositeKey, MapWritable, Text, Text> {
	@Override
	public void reduce(AirlineMonthCompositeKey key, Iterable<MapWritable> values, Context context)
			throws IOException, InterruptedException {

		int currMonth = 1; // Start with first month
		int sum = 0;
		int count = 0;
		int avg = 0;

		// used to make sure currMonth is in sync with month - to fill in gaps
		int monthValidator = 1;

		StringBuilder finalResult = new StringBuilder();

		int month = 1;
		boolean isFlightActiveIn2015 = false;
		int totalAirlineFrequency = 0;

		finalResult.append(key.getAirLineName().toString() + ",");
		for (MapWritable mapVal : values) {

			/* Checks if airline is active in 2015 */
			int year = ((IntWritable) mapVal.get(new IntWritable(2))).get();
			if (year == 2015 || isFlightActiveIn2015) {
				isFlightActiveIn2015 = isFlightActiveIn2015 || true;
			}

			count++;
			totalAirlineFrequency++;
			month = Integer.parseInt(key.getMonth().toString());

			// when ever we get the values for the next month, we write the
			// result and reset the counters
			if (currMonth != month) {
				avg = sum / count;
				finalResult.append(currMonth + "," + avg + ",");
				sum = 0;
				count = 0;
				currMonth = month;
				monthValidator++;

				// if we miss a month in the data we still write it to the file
				// but put average as empty
				if (monthValidator != currMonth) {
					for (int i = monthValidator; i < currMonth; i++) {
						finalResult.append(i + "," + 0 + ",");
					}
					monthValidator = currMonth;
				}
			}
			sum += ((IntWritable) mapVal.get(new IntWritable(1))).get();
		}

		// appending the last month values
		avg = sum / count;
		finalResult.append(month + "," + avg + ",");

		// when there are no records for all months, we write the average price
		// as zero
		while (currMonth < 12) {
			finalResult.append(++currMonth + "," + 0 + ",");
		}

		/*
		 * NOTE :- Our Design Assumption - Only if flight is active in 2015 then
		 * only Output the data
		 */
		if (isFlightActiveIn2015) {
			finalResult.append(totalAirlineFrequency);
			// finally writing the output to context
			context.write(new Text(), new Text(finalResult.toString()));
		}

	}
}
