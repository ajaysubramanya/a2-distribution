
/**
 * @author ajay subramanya & smitha bangalore naresh
 * @date 01/29/2016
 * @info Assignment 3
 * Compares the composite key.
 * First by Airline Name then by Month
 * Used by setSortComparatorClass
 * */
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class AirlineMonthCompositeKeyComparator extends WritableComparator {
	/**
	 * Constructor.
	 */
	protected AirlineMonthCompositeKeyComparator() {
		super(AirlineMonthCompositeKey.class, true);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2) {
		AirlineMonthCompositeKey a1 = (AirlineMonthCompositeKey) w1;
		AirlineMonthCompositeKey a2 = (AirlineMonthCompositeKey) w2;

		int result = a1.getAirLineName().compareTo(a2.getAirLineName());
		if (result == 0) {
			int m1 = Integer.parseInt(a1.getMonth().toString());
			int m2 = Integer.parseInt(a2.getMonth().toString());
			result = (m1 == m2 ? 0 : (m1 < m2 ? -1 : 1));
		}
		return result;
	}
}
