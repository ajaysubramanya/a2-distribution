
/**
 * @author ajay subramanya & smitha bangalore naresh
 * @date 01/29/2016
 * @info Assignment 3
 * Partitions key based on "natural" key (i.e. AirlineName)
 * Used during reduce phase
 * */
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.mapreduce.Partitioner;

public class AirlineNamePartitioner extends Partitioner<AirlineMonthCompositeKey, MapWritable> {

	@Override
	public int getPartition(AirlineMonthCompositeKey aKey, MapWritable aVal, int numOfPartitions) {

		int hash = aKey.getAirLineName().hashCode();
		int partition = hash % numOfPartitions;
		return partition;

	}

}
