
/**
 * @author ajay subramanya & smitha bangalore naresh
 * @date 01/29/2016
 * @info Assignment 3
 * Groups values by Airline Name
 * So all similar airlines from Map will go to one reducer, since
 * we have our key has <AirlineName, Month> we do not want it to
 * be split be <AirlineName, Month>
 * Used by setGroupingComparatorClass
 * Used during reduce phase
 * */
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class AirlineNameGroupingComparator extends WritableComparator {
	protected AirlineNameGroupingComparator() {
		super(AirlineMonthCompositeKey.class, true);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2) {
		AirlineMonthCompositeKey a1 = (AirlineMonthCompositeKey) w1;
		AirlineMonthCompositeKey a2 = (AirlineMonthCompositeKey) w2;

		return a1.getAirLineName().compareTo(a2.getAirLineName());
	}

}
