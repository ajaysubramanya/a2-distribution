
/**
 * @author ajay subramanya & smitha bangalore naresh
 * @date 01/29/2016
 * @info Assignment 3
 * Composite Key class - Used for secondary sorting
 * The "natural" key is the AirlineName. The secondary sort will be performed
 * against the Month.
 * */
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class AirlineMonthCompositeKey implements WritableComparable<AirlineMonthCompositeKey> {
	private Text airLineName;
	private Text month;

	/**
	 * default constructor
	 */
	public AirlineMonthCompositeKey() {

	}

	/**
	 * Parameterized constructor
	 *
	 * @param airLineName
	 *            : the name of the airline. Ex : AA
	 * @param month
	 *            : the month of flight schedule for the particular airline. Ex
	 *            : 1
	 */
	public AirlineMonthCompositeKey(Text airLineName, Text month) {
		this.airLineName = airLineName;
		this.month = month;
	}

	/**
	 * Getters and setters
	 */
	public Text getAirLineName() {
		return airLineName;
	}

	public void setAirLineName(Text airLineName) {
		this.airLineName = airLineName;
	}

	public Text getMonth() {
		return month;
	}

	public void setMonth(Text month) {
		this.month = month;
	}

	@Override
	/**
	 * Deserialize the fields of this object from in.
	 */
	public void readFields(DataInput in) throws IOException {

		if (this.airLineName == null)
			this.airLineName = new Text();

		if (this.month == null)
			this.month = new Text();

		this.airLineName.readFields(in);
		this.month.readFields(in);
	}

	@Override
	/**
	 * Serialize the fields of this object to out.
	 */
	public void write(DataOutput out) throws IOException {
		this.airLineName.write(out);
		this.month.write(out);
	}

	public int compare(AirlineMonthCompositeKey o2) {
		return this.getAirLineName().compareTo(o2.getAirLineName());
	}

	/* Necessary for comparator to work */
	@Override
	public boolean equals(Object o2) {
		return this.getAirLineName().equals(((AirlineMonthCompositeKey) o2).getAirLineName());
	}

	/*
	 * First compare by Airline Name , if equal then compare by Month
	 */
	@Override
	public int compareTo(AirlineMonthCompositeKey o2) {
		int result = this.getAirLineName().compareTo(o2.getAirLineName());
		if (result == 0) {
			int m1 = Integer.parseInt(getMonth().toString());
			int m2 = Integer.parseInt(o2.getMonth().toString());
			result = (m1 == m2 ? 0 : (m1 < m2 ? -1 : 1));
		}
		return result;
	}

	@Override
	public int hashCode() {
		return this.getAirLineName().hashCode();
	}

	@Override
	public String toString() {
		return (new StringBuilder()).append(this.getAirLineName()).append(',').append(this.getMonth()).toString();
	}
}
