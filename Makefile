pseudo:	
	#mr-jobhistory-daemon.sh stop historyserver     - NOTE : To be stopped before        	#stop-yarn.sh					- NOTE : To be stopped before
	#stop-dfs.sh									- NOTE : To be stopped before											  	
	hdfs namenode -format									   #formatting the name node 
	start-dfs.sh										   #starting the hadoop file system 	
	start-yarn.sh										   #starting YARN - resource manager	
	mr-jobhistory-daemon.sh start historyserver						   #starting MapReduce 
	
	#hadoop fs -rm -r outputAjSm									   #NOTE - deleting any previous output that may be in hadoop fs 
	#hadoop fs -rm -r inputAjSm									   #NOTE - deleting any previous input that may be in hadoop fs 	
	
	hadoop fs -mkdir inputAjSm							   #making directory to place input
	hadoop fs -put all/* inputAjSm						   #putting all the input files on the HDFS - NOTE all with input files has to be in current directory

	hadoop jar AvgPricePerMonthCal.jar inputAjSm outputAjSm					   #running the JAR that we created earlier - NOTE jar has to be in current directory

	hadoop fs -get outputAjSm 
	cat outputAjSm/* > Results.csv
	Rscript GraphsScript.R


cloud:
	#aws s3 rm s3://mrsmithaajaybuk/AvgPricePerMonthCal.jar
	#aws s3 mb s3://mrsmithaajaybuk
	aws s3 cp AvgPricePerMonthCal.jar s3://mrsmithaajaybuk
	aws emr create-cluster  --name "A2-Distribution-Cluster" \
	  --ami-version 3.10.0  \
	  --instance-groups InstanceGroupType=MASTER,InstanceCount=1,InstanceType=m3.xlarge \
	             	InstanceGroupType=CORE,InstanceCount=1,InstanceType=m3.xlarge \
	  --steps Type=CUSTOM_JAR,Name="STEP1",ActionOnFailure=CONTINUE,\
Jar=s3://mrsmithaajaybuk/AvgPricePerMonthCal.jar,MainClass=AvgPricePerMonthCal,\
Args=[-input=s3://mrsmithaajaybuk/input,\
-output=s3://mrsmithaajaybuk/outputDir,\
-fsroot=s3://mrsmithaajaybuk/,\
-awskeyid=$(AWS_ACCESS_KEY),\
-awskey=$(AWS_SECRET_KEY)] \
	--auto-terminate \
	--log-uri s3://mrclassvitek/log \
	--service-role EMR_DefaultRole \
	--ec2-attributes InstanceProfile=EMR_EC2_DefaultRole,AvailabilityZone=us-west-2 \
	--enable-debugging


cloud1:
	java -jar aws_setup.jar input/ AvgPricePerMonthCal.jar  "please_enter_your_key_pair_here"






